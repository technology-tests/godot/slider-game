# Instructions

1. Download `src` directory from this repository
    - [Direct link for `.zip`](https://gitlab.com/technology-tests/godot/slider-game/-/archive/main/slider-game-main.zip?path=src)
1. [Download Godot Engine](https://godotengine.org/download/)
    - [Direct link for 64-bit Windows](https://github.com/godotengine/godot/releases/download/4.0.1-stable/Godot_v4.0.1-stable_win64.exe.zip)
    - [Direct link for 64-bit Linux](https://github.com/godotengine/godot/releases/download/4.0.1-stable/Godot_v4.0.1-stable_linux.x86_64.zip)
    - [Direct link for macOS](https://github.com/godotengine/godot/releases/download/4.0.1-stable/Godot_v4.0.1-stable_macos.universal.zip)
1. Start Godot Engine
1. Import project from `src/project.godot`
1. Run project (<kbd>F5</kbd>, or "run" icon at the top right)
