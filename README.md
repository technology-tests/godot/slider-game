Demo
===

It is recommended to run natively with Godot Engine.
For detailed instructions, view [`docs/running.md`](https://gitlab.com/technology-tests/godot/slider-game/-/blob/main/docs/running.md).

A web demo is provided (link in description), but some browsers and platforms are not supported.


Gallery
---

![](https://gitlab.com/technology-tests/godot/slider-game/-/raw/main/docs/sample.mp4)
