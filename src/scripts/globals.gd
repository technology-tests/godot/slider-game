# Based on https://docs.godotengine.org/en/4.0/tutorials/scripting/singletons_autoload.html#creating-the-script
extends Node

var current_scene = null
@onready var background_music_player = get_node_or_null("/root/Root/BackgroundMusicPlayer")
var background_music_volume = 25.00

const TILE_SIZE = 64


func _ready():
	var root = get_tree().root
	var root_scene = root.get_child(root.get_child_count() - 1)
	current_scene = root_scene.get_child(0)
	set_background_music_volume(background_music_volume)


func goto_scene(path):
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	call_deferred("_deferred_goto_scene", path)


func _deferred_goto_scene(path):
	# It is now safe to remove the current scene
	current_scene.free()

	# Load the new scene.
	var s = ResourceLoader.load(path)

	# Instance the new scene.
	current_scene = s.instantiate()

	# Add it to the active scene, as child of root.
	get_tree().root.add_child(current_scene)

	# Optionally, to make it compatible with the SceneTree.change_scene_to_file() API.
	get_tree().current_scene = current_scene


func set_background_music_volume(value: float):
	# With "run current scene", the root scene isn't instantiated
	if background_music_player:
		background_music_player.volume_db = linear_to_db(value / 25.00)
		background_music_volume = value


func check_camera_centered(camera: Camera2D, tilemap: TileMap):
	var tilemap_rect = tilemap.get_used_rect()

	var x_size = tilemap_rect.size.x * tilemap.tile_set.tile_size.x
	var y_size = tilemap_rect.size.y * tilemap.tile_set.tile_size.y

	var x_pos = tilemap_rect.position.x * tilemap.tile_set.tile_size.x
	var y_pos = tilemap_rect.position.y * tilemap.tile_set.tile_size.y

	var viewport = camera.get_viewport()
	var viewport_rect = viewport.get_visible_rect()

	var desired_camera_position = Vector2(
		-(viewport_rect.size.x / camera.zoom.x - x_size) / 2.00 + x_pos,
		-(viewport_rect.size.y / camera.zoom.y - y_size) / 2.00 + y_pos,
	)

	var epsilon = 0.10
	assert(
		camera.position.x - desired_camera_position.x < epsilon
		and
		camera.position.y - desired_camera_position.y < epsilon,
		"Camera position should be at %s to avoid flicker when loading level,
		but was at %s" % [desired_camera_position, camera.position]
	)


func snap_position(node: Node, tilemap: TileMap):
	var tilemap_rect = tilemap.get_used_rect()

	var node_tilemap_relative_offset = node.position - Vector2(tilemap_rect.position * TILE_SIZE)
	var node_position_snapped = node_tilemap_relative_offset.snapped(Vector2.ONE * TILE_SIZE / 2.00)
	node.position = node_position_snapped + Vector2(tilemap_rect.position * TILE_SIZE)
