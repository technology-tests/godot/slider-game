extends Node


func _ready():
	restart_background_music()


func _on_background_music_finished():
	restart_background_music()


func restart_background_music():
	$BackgroundMusicPlayer.play(0.00)
