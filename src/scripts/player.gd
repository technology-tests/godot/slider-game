extends CharacterBody2D

const SLIDE_SPEED = 750

@onready var moved = false
var collision_object = null

func _physics_process(delta):
	# Ensure there is at least one frame where velocity is zero
	# between movements to check if player has reached the goal
	if collision_object:
		collision_object = null
		velocity = Vector2.ZERO
		return

	if velocity == Vector2.ZERO:
		if Input.is_action_pressed("ui_left"):
			velocity.x = -SLIDE_SPEED
			moved = true
		elif Input.is_action_pressed("ui_right"):
			velocity.x = SLIDE_SPEED
			moved = true
		elif Input.is_action_pressed("ui_up"):
			velocity.y = -SLIDE_SPEED
			moved = true
		elif Input.is_action_pressed("ui_down"):
			velocity.y = SLIDE_SPEED
			moved = true

	if velocity.length():
		collision_object = move_and_collide(velocity * delta)
