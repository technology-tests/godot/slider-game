class_name Level
extends Node2D

@onready var tilemap = $TileMap
@onready var player = $Player
@onready var goal = $Goal
@onready var camera = $Camera2D
var next_scene = null


func _ready():
	Globals.snap_position(player, tilemap)
	Globals.snap_position(goal, tilemap)

	Globals.check_camera_centered(camera, tilemap)


func _process(_delta):
	if $Player.velocity == Vector2.ZERO:
		# Snap player to grid position after movement
		# to fix physics collision inaccuracies
		Globals.snap_position(player, tilemap)

		if player.position == goal.position:
			# Prevent player movement after reaching goal
			# for a short amount of time for scene transition
			player.set_physics_process(false)
			await get_tree().create_timer(0.50).timeout

			Globals.goto_scene(next_scene)
