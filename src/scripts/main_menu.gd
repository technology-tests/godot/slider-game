extends Control


func _on_settings_button_pressed():
	Globals.goto_scene("res://scenes/settings.tscn")


func _on_start_game_button_pressed():
	Globals.goto_scene("res://scenes/levels/1.tscn")
