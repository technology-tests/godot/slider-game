extends Control


func _ready():
	find_child("BackgroundMusicVolumeSlider").value = Globals.background_music_volume


func _on_return_button_pressed():
	Globals.goto_scene("res://scenes/main_menu.tscn")


func _on_background_music_volume_slider_value_changed(value):
	Globals.set_background_music_volume(value)
